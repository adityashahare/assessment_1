const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs/promises'); // Using the promises version of the 'fs' module
const app = express();
const port = 3001;

app.use(bodyParser.urlencoded({ extended: true }));

const dataFilePath = 'data.json';

// Load existing data from the file, if it exists
let storedData = [];

fs.readFile(dataFilePath, 'utf8')
  .then((data) => {
    storedData = JSON.parse(data);
  })
  .catch((error) => {
    console.error('Error reading data file:', error.message);
  });

app.get('/', (req, res) => {
  res.send(`
    <form method="post" action="/submit">
      <label for="name">Name:</label>
      <input type="text" id="name" name="name" required>
      <br>
      <label for="email">Email:</label>
      <input type="email" id="email" name="email" required>
      <br>
      <button type="submit">Submit</button>
    </form>
  `);
});

app.post('/submit', (req, res) => {
  const { name, email } = req.body;
  const newData = { name, email };
  storedData.push(newData);

  // Save the updated data to the file
  fs.writeFile(dataFilePath, JSON.stringify(storedData))
    .then(() => {
      res.send('Form submitted successfully!');
    })
    .catch((error) => {
      console.error('Error writing to data file:', error.message);
      res.status(500).send('Internal Server Error');
    });
});

app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});